from django.shortcuts import render
from .models import GPSUpdate, TestRoute, GPSFilter, Driver
from rest_framework import generics, views
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import GPSUpdateSerializer, DriverSerializer
import datetime

from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer, BrowsableAPIRenderer

# TODO I don't like this coupling
from restexample.permissions import IsOwnerOrReadOnly

from logging import warn

class DriverViewSet(viewsets.ModelViewSet):
    """
    API Endpoint for Drivers
    """
    
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

    filter_fields = ('user', 'inventory')
    
# TODO: Limit GPS to Read and Create
class GPSDataViewSet(viewsets.ModelViewSet):
    """
    API Endpoint for GPS Data
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          )
                          
    queryset = GPSUpdate.objects.all()
    serializer_class = GPSUpdateSerializer

    filter_class = GPSFilter
    
    ''' passing the kwargs doesnt work?
    def get_queryset(self):
        for_today = self.kwargs.get('today', None);
        warn("Today %s", for_today)
        if for_today:
          today = datetime.date.today()
          tomorrow = today + datetime.timedelta( days = 1 )
          queryset = GPSUpdate.objects.all().exclude( created__lt = today ).exclude( created__gt = tomorrow )
          warn("Queryset for today %s until %s length %d", today, tomorrow, len(queryset))
        else:
          queryset = GPSUpdate.objects.all();
        return queryset
    '''

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        
class TodaysGPSMap(generics.ListAPIView):
    '''
    Today's GPS points
    '''
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          )
                          
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    serializer_class = GPSUpdateSerializer
        
    def get_queryset(self):
          today = datetime.date.today()
          tomorrow = today + datetime.timedelta( days = 1 )
          queryset = GPSUpdate.objects.all().exclude( created__lt = today ).exclude( created__gt = tomorrow )
   
          warn("Queryset for today %s until %s length %d", today, tomorrow, len(queryset))
          return queryset
          


def filter_gps_list(request):
    f = GPSFilter(request.GET, queryset=GPSUpdate.objects.all())
    return render(request, 'gps/filter.html', {'filter': f})
    
