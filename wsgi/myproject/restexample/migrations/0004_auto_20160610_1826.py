# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restexample', '0003_auto_20160610_1823'),
    ]

    operations = [
        migrations.RenameField(
            model_name='foo',
            old_name='might_be_blank',
            new_name='might_be_blank_not_req',
        ),
        migrations.RenameField(
            model_name='foo',
            old_name='required_woth_default',
            new_name='some_default_value_not_req',
        ),
    ]
