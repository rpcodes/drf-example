from django.conf.urls import url, include
from django.views.generic import TemplateView
from rest_framework import routers
from restexample import views
import gps.views
import gps.urls
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.authtoken import views as token_views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'customers', views.CustomerViewSet)
router.register(r'foo', views.FooViewSet)
router.register(r'bar', views.BarViewSet)
router.register(r'gps', gps.views.GPSDataViewSet )
router.register(r'driver', gps.views.DriverViewSet )


# Template view style...
urlpatterns = []

'''
from rest_framework.urlpatterns import format_suffix_patterns

#Example for List and Detail TemplateHTMLViews
urlpatterns = [
    url(r'^contacts/$', views.ContactList.as_view(), name='contact-list'),
    url(r'^contacts/(?P<pk>[0-9]+)/$', views.ContactDetail.as_view(), name='contact-detail'),
    
]

urlpatterns = format_suffix_patterns(urlpatterns)

'''

urlpatterns += [
    # just tpdays GPS
    url(r'^api/gps/today/$', gps.views.TodaysGPSMap.as_view() ),

    
    url(r'^api/', include(router.urls, namespace="api")),
    
    # GPS APP
    url(r'^gps/', include(gps.urls)),
]  +  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# Token Auth
urlpatterns += [
    url(r'^api-token-auth/', token_views.obtain_auth_token)
]

# custom log in or out
from django.contrib.auth import views as rest_auth

template_name = {'template_name': 'backoffice/login.html'}

urlpatterns += [
    url(r'^api-auth/login/$', rest_auth.login, template_name, name='login'),
    url(r'^api-auth/logout/$', rest_auth.logout, template_name, name='logout'),
]

# Uses API endpoints and JS to interact with system
urlpatterns += [

    url(r'map/$', TemplateView.as_view(template_name="gps/drivers.html"), name="map"),
    url(r'^$', TemplateView.as_view(template_name="backoffice/index.html")),
    url(r'^getfoo/(?P<pk>[0-9]+)/$', views.FooDetail.as_view()),
    url(r'^listfoo/$', views.FooList.as_view()),

]