# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restexample', '0005_auto_20160610_1901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bar',
            name='foo',
            field=models.ForeignKey(to='restexample.Foo', related_name='bars'),
        ),
    ]
