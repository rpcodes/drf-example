# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restexample', '0004_auto_20160610_1826'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('might_be_blank_not_req', models.CharField(blank=True, max_length=100)),
                ('some_default_value_not_req', models.CharField(default='default', max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='foo',
            name='might_be_blank_not_req',
        ),
        migrations.RemoveField(
            model_name='foo',
            name='some_default_value_not_req',
        ),
        migrations.AddField(
            model_name='bar',
            name='foo',
            field=models.ForeignKey(to='restexample.Foo'),
        ),
    ]
