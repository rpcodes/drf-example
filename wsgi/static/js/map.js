
/**
* Javascript to manage API calls to GPS endpoints
* and render them onto a map.
* Currently uses 
*
* @module map
*/

/** @namespace Namespace for MYAPP classes and functions. */
var _map = _map || (function(){
    // TODO Get from HTML element when logged in
    const API_KEY = "";
    // todo: wrap function calls to handle different geolocation services; also get from HTML element via auth
    const engine = "";
    
    // change to make default map start location
      var geolocateUserIsOn = true;
      
      // instead track the size of the list below ... otherwise will lose track var lastIndex = 0; // {{ points|length }}
      var currentGpsPoints = []; // TODO: Could this become too large? limit stored results
      // Map Object
      // Create map Object
      function createMapObject(args) {
        var thelat, thelon;
        
        if ( args["lat"]) {
            thelat = args["lat"];
        } else {
       
            thelat = 39.6638;
        }
        if ( args["lon"]) {
            thelon = args["lon"];
        } else {
            thelon = -75.0775;
        }
       var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: thelat, lng: thelon},
            zoom: 10
            , streetViewControl: false
          });
       
       return map;
      }
      
      // PUBLIC 
      function initMap(args) {
        var d = {};
        if (!args) {
            d["lat"] = 39.6638;
            d["lon"] = -75.0775;
        } else {
            d = args;
        }
        mapObj = createMapObject(d);
        console.log("Created map object.");
        
      }
      
      // give it a place.geometry.location
      // location can be {lat:lat,lon:lon}
      function doMarker(location, icon1) {
         markersArray.push(new google.maps.Marker({
              map: mapObj,
              position: location,
              icon: icon1
            }));
      }
      
      
   /* for paginated data, we need to get data from results key
    returns false if an error in storing data */
    function process_results(data) {
        var processedOK = false;
        var results = data['results']
        if (results != null) {
            //code
            console.log(results);
            processedOK = store_gps_data(results);
        } else {
            processedOK = store_gps_data(data);
        }
        return processedOK;
    }
    
    /* store recent gps objects, and list size,
     so we can monitor the list size for the ?offset query arg
     TODO: key error handling
     TODO: Shoudlnt this be show data?
     */
    function store_gps_data(data) {
        var i = 0;
        var loc;
        for (i=0; i < data.length; i++){
            if (typeof data[i]['lat'] != 'undefined'
                && typeof data[i]['lon'] != 'undefined') {
                loc = {lat:data[i]['lat'],lng:data[i]['lon']}
                // TODO: Manage updates and avoid re-drawing all points
                // set location as gps points
                currentGpsPoints.push(loc)
            } else {
                console.log("Malformed GPS data from our server; see console log." )
            }
        }
        // draw lines
        if (typeof currentGpsPoints != 'undefined') {
            
            console.log("Currently " + currentGpsPoints.length + " points")
            drawPolylineFromPoints(currentGpsPoints)
        }
        // mapObj.setCenter(loc);
        return true;
    }
    
    /* draw a polyline with the given points */
    function drawPolylineFromPoints(points) {
        var flightPath = new google.maps.Polyline({
            path: points,
            geodesic: true,
            strokeColor: '#CC3333',
            strokeOpacity: 1.0,
            strokeWeight: 3
        });

        flightPath.setMap(mapObj);
    }
    
    function helperTimeRange() {
        //Get the start and end times, if
        // not defined, then add the defaults
        // TODO: get and add defaults from elements in DOM
        var rex = /[TZ]/g; // strip off T and Z in ISO format for compat w django-filter
        var d1 = new Date(Date.now()).toISOString().replace(rex," "); //now
        var ONE_HOUR = 60 * 60 * 1000; /* ms */
        // minus one hour for start
        var d0 = new Date(Date.now() - ONE_HOUR).toISOString().replace(rex," ");
        console.log(new Date().toISOString() + "::: " + d0 + " to " + d1)
        return {'start' : d0, 'end' : d1 };
    }
    
    /* Load GPS points */
    function loadPoints() {
        // google.maps.event.trigger(mapObj, 'resize');
        // skip ones we already have
        var lastIndex = currentGpsPoints.length
        var url = "/api/gps/"; //?limit=1000&offset=" + lastIndex
        //var url = "http://127.0.0.1:8000/gps/data.json/?limit=50";
        
        console.log(url)
        // Assign handlers immediately after making the request,
        // and remember the jqXHR object for this request
        var queryOptions = {};
        queryOptions['limit'] = 1000;
        queryOptions['offset'] = lastIndex;
        var startend = helperTimeRange();
        queryOptions['created_0'] = startend['start'];
        queryOptions['created_1'] = startend['end'];
        
        var jqxhr = $.ajax( url
            , { jsonp : false
                , dataType : "json"
                , contentType: "application/json;"
                , data : queryOptions
            } )
          .done(function(data) {
            var ok = process_results(data);
            if (ok) {
                //put onto map
                // doMarker(currentGpsPoints)
            } else {
                console.log("Error");
                // TODO Indicate error to user
            }
            
          })
          .fail(function() {
            console.log( "error" );
          })
          .always(function() {
            console.log( "complete" );
          });
    }
    
    return {
      
         initMap : initMap
         , loadPoints : loadPoints
       }
    
})();