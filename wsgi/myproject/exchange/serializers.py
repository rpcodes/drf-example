class GPSUpdateSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = GPSUpdate
        fields = ('created','owner', 'lat','lon','mph','sat') 