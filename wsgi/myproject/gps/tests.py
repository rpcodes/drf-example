from django.test import TestCase
from django.contrib.auth.models import User

import requests
from rest_framework.authtoken.models import Token

class GPSDataTest(TestCase):
    def setUp(self):
        # Include an appropriate `Authorization:` header on all requests.
        self.user = User(username="test",password="testpass")
        self.user.save();
        self.token = Token.objects.get(user__username='test')
        
        # we could use APICLient here instead
        
    def test_post_gps_pass(self):
        r = requests.post("http://127.0.0.1:8000/gps/data/"\
        ,  json={"mph":0,"lat":1.2,"lon":1.2}\
        ,headers={"content-type" : "application/json"\
        , "authorization" : "token " + self.token.key})
        self.assertEquals(r.status_code == 201)

    
    