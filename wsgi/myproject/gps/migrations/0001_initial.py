# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GPSData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('lat', models.FloatField()),
                ('lon', models.FloatField()),
                ('sat', models.IntegerField(default=-1)),
                ('mph', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='TestRoute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('route_name', models.CharField(max_length=512)),
                ('lat_lon_list', models.CharField(max_length=4096)),
                ('distances', models.CharField(max_length=4096)),
            ],
        ),
    ]
