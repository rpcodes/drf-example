from django.db import models
from django.core.validators import RegexValidator

# generate token for each user
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        
class Contact(models.Model):
    '''
    Stores basic info for a person
    '''
    created = models.DateTimeField(auto_now_add=True)
    # Name
    first_name = models.CharField(max_length=100, blank=False, default='')
    middle_name = models.CharField(max_length=100, blank=False, default='')
    last_name = models.CharField(max_length=100, blank=False) #REQD
    # Phone
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=16, blank=True) # validators should be a list
    # Email
    email = models.EmailField(max_length=256, blank=True)
    # US Street Address
    billing_address = models.CharField(max_length=512, blank=False)
    # Company name
    # company_name = models.CharField(max_length=100, default='', blank=True)
    # Owned by its creator
    owner = models.ForeignKey('auth.User', related_name='contacts')
               
    class Meta:
        ordering = ('last_name','first_name','middle_name')
   
    def __str__(self):
       return ""+self.last_name +','+self.first_name+' '+self.last_name;
     
class Company(models.Model):
    name = models.CharField(max_length=100, blank=False)
    owner_contact = models.OneToOneField(
        Contact
        , on_delete=models.CASCADE
        )     
    
    class Meta:
        ordering = ('name',)
   
class ExchangeSite(models.Model):
    # address
    company = models.ForeignKey(Company, blank=False)
    # TODO: how to repr employees
    
class Foo(models.Model):
    '''
    Test
    '''
    created = models.DateTimeField(auto_now_add=True)
    foo = models.CharField(max_length=100, blank=False)
    
    # Owned by its creator
    owner = models.ForeignKey('auth.User', related_name='foo')
    class Meta:
        ordering = ['created']
        
    def __unicode__(self):
        return u"Foo: " + self.foo
        
    def __str__(self):
        return u"Foo: " + self.foo
        

class Bar(models.Model):
    '''
    FK test
    '''    
    created = models.DateTimeField(auto_now_add=True)
    
    foo = models.ForeignKey(Foo, related_name="bars")
    
    might_be_blank_not_req = models.CharField(max_length=100, blank=True)
    # this isnt really required if it has a default
    some_default_value_not_req = models.CharField(max_length=100, blank=False, default="default")
