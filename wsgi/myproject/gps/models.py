from django.db import models

'''
Used for the center of the map placement, as well as 
for exchange site locations, complementary to the address
Also used by Drivers when tracking GPS coordinates
'''
'''
TODO:
Is there enough of a benefit to use this format? I dont think so
class GPSLocation(models.Model):
    lat = models.FloatField(blank=False,null=False)
    lon = models.FloatField(blank=False,null=False)
    
'''

class GPSLocation(models.Model):
    lat = models.FloatField(blank=False,null=False)
    lon = models.FloatField(blank=False,null=False)
    
'''
For periodically storing GPS info while tracking drivers

TODO
Drivers, Admins, should be able to create these
For now, we let any User add it under their user name (owner field)
But should also check if that user is a Driver (or admin) before
allowing (perms)
'''
class GPSUpdate(GPSLocation):
    created = models.DateTimeField(auto_now_add=True)
    # lat lon    
    #lat = models.FloatField(blank=False,null=False)
    #lon = models.FloatField(blank=False,null=False)
    # location = models.ForeignKey('GPSLocation', related_name='gpslocation')    
    # Number of satellites
    sat = models.IntegerField(default=-1);
    # mph -1.0f if unset
    mph = models.FloatField(blank=False,null=False);

    owner = models.ForeignKey('auth.User', related_name='gpsdata')
    
    def __str__(self):
        return "("+str(self.lat)+","+str(self.lon)+") " + str(self.created)
'''
Condensed form of route
name
TODO: address object for origin and destination, i.e. GPS
String for the lat lon pairs
string for the distance between each (TODO: in mi or km????)
'''
class TestRoute(models.Model):
    route_name = models.CharField(max_length=512)
    # lat,lon lat,lon lat,lon (as floats)
    lat_lon_list = models.CharField(max_length=4096,blank=False,null=False)
    # d d d (as floats)
    distances = models.CharField(max_length=4096,blank=False,null=False)
    

'''
Driver Info
Must be associated with a User with the Driver role

'''
class Driver(models.Model):
     user = models.ForeignKey('auth.User', related_name='driver_auth')
     color = models.CharField(max_length=7, default="#009900") # "#RRGGBB" color value, for gps map
     # TODO: May need to be more sophisticated here
     inventory = models.IntegerField(default=0)
     # TODO Route, delivery, times
     
# FILTERS
    
import django_filters

class GPSFilter(django_filters.FilterSet):

   created =  django_filters.DateTimeFromToRangeFilter()
   owner = django_filters.CharFilter(name='owner__username')
   class Meta:
        model = GPSUpdate
        fields = ['created','owner']