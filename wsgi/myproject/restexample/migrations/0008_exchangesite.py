# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restexample', '0007_auto_20160614_0122'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExchangeSite',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('company', models.ForeignKey(to='restexample.Company')),
            ],
        ),
    ]
