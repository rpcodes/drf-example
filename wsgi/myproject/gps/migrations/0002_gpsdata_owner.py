# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gps', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gpsdata',
            name='owner',
            field=models.ForeignKey(default=1, related_name='gpsdata', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
