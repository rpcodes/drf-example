from django.contrib.auth.models import User, Group
from .models import Contact, Foo, Bar

from rest_framework import serializers
from logging import warn

import pdb; 


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'last_name','first_name', 'email', 'groups', 'password')

        # thanks to http://stackoverflow.com/questions/27468552/changing-serializer-fields-on-the-fly/#answer-27471503
        extra_kwargs = {
            'password': {
                'write_only': True,
            },
        }
        
#    def create(self, validated_data):
#        warn("Password " + validated_data['password'])
#        g_data = validated_data.pop('groups')        
#        user = User.objects.create_user(**validated_data)
#        user.groups = g_data        
#        user.save()
#        return user


class GroupSerializer(serializers.ModelSerializer):
    group_id = serializers.ReadOnlyField(source='id')   
    class Meta:
        model = Group
        fields = ('name', 'group_id')

class ContactSerializer(serializers.ModelSerializer):
    # doing this means we have to handle it outside
    owner = serializers.ReadOnlyField(source='owner.username')   
    class Meta:
        model = Contact
        fields = '__all__' #('last_name','first_name','middle_name'
        #,'billing_address','company_name','phone_number')

class BarSerializer(serializers.ModelSerializer):
    '''
    NOTE we need create and update methods if using fk read only = false
    '''    

    # This shows us the Foo.foo to repr the Foo connection Bar -fk-> Foo    
    foo = serializers.SlugRelatedField(many=False, read_only=False,\
    queryset=Foo.objects.all(), slug_field="foo")

    class Meta:
        model = Bar
        fields = ('foo','might_be_blank_not_req', 'some_default_value_not_req') #('foo', 'owner') #TODO: Could we use _all_ and depth =1 instread?


        
class FooSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')   
    bars =  BarSerializer(many=True);

    # reverse rel, if model is
    #album = models.ForeignKey(Album, related_name='tracks')
    # then rel name tracks goes into meta fields
    # class AlbumSerializer(serializers.ModelSerializer):
    #class Meta:
    #    fields = ('tracks', ...)
    class Meta:
        model = Foo
        # 'bars' is the related name pointing from bar to foo
        fields = ('bars', 'foo', 'owner') #('__all__') wont work for reverse relationships

    def create(self, validated_data):
        bar_data = validated_data.pop('bars')
        foo = Foo.objects.create(**validated_data)
        for bar in bar_data:
            Bar.objects.create(foo=foo, **bar_data)
        return foo

    def update(self, instance, validated_data):
        bars_data = validated_data.pop('bars')
                
        return instance;
    
