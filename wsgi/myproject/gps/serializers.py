# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 23:53:36 2016

@author: page
"""
from rest_framework import serializers
from .models import GPSUpdate, Driver, GPSFilter
from restexample.serializers import UserSerializer
from django.contrib.auth.models import User
from logging import warn

class GPSUpdateSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = GPSUpdate
        fields = ('created','owner', 'lat','lon','mph','sat') 
    
class DriverSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True) #serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Driver
        fields = ('color','user','inventory')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        groups = user_data.pop('groups')
        
        u = User.objects.create_user(**user_data)
        u.groups = groups
        
        d = Driver.objects.create(user=u, **validated_data)
        return d

