# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gps', '0002_gpsdata_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='GPSUpdate',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('lat', models.FloatField()),
                ('lon', models.FloatField()),
                ('sat', models.IntegerField(default=-1)),
                ('mph', models.FloatField()),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='gpsdata')),
            ],
        ),
        migrations.RemoveField(
            model_name='gpsdata',
            name='owner',
        ),
        migrations.DeleteModel(
            name='GPSData',
        ),
    ]
