from django.views.generic import TemplateView
from django.conf.urls import url
from gps import views
from rest_framework.urlpatterns import format_suffix_patterns

# GPS App related URLS

# TODO: this really could be a vuew
urlpatterns = [
    #url(r'map/', views.TodaysGPSMap.as_view(), name="map")
    url(r'map/', TemplateView.as_view(template_name="gps/drivers.html"), name="map"),
    url(r'today/', views.TodaysGPSMap.as_view()),
    url(r'filter/', views.filter_gps_list),
    url(r'beacon/', TemplateView.as_view(template_name="gps/gps.html"), name="beacon"),



    # TODO Move this

    
    url(r'newdriver/', TemplateView.as_view(template_name="backoffice/newemployee.html"), name="newdriver"),
    url(r'newadmin/', TemplateView.as_view(template_name="backoffice/newadmin.html"), name="newadmin"),
    
]
# above only works when Debug=True


