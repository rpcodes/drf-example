# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gps', '0004_auto_20160627_1930'),
    ]

    operations = [
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('first_name', models.CharField(max_length=128)),
                ('inventory', models.IntegerField(default=0)),
                ('user', models.ForeignKey(related_name='driver_auth', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
