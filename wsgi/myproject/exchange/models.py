from django.db import models


# Create your models here.
'''
TODO: Having one to Many for Owner Accounts?
As well as Attendant accounts?
'''
class Customer(models.Model):
    business_name = models.CharField(max_length=1024,blank=False)

'''
Holds associations to Customer/Location data
by matching the user account(s) to th Customer
'''
class CustomerOwnerData(models.Model):
    user = models.ForeignKey('auth.User')
    customer = models.ForeignKey('exchange.Customer')
    
'''

'''
class Location(models.Model):
    '''
    This is a temporary field...
    '''
    address_as_entered = models.CharField(max_length=1024,blank=False)
    '''
    Locations are owned by a customer
    '''
    customer = models.ForeignKey('exchange.Customer')
    

class InventoryUpdate(models.Model):
    
    created = models.DateTimeField(auto_now_add=True)
    
    full_bottles = models.IntegerField(default=0, blank=False)
    
    location = models.ForeignKey('exchange.Location')