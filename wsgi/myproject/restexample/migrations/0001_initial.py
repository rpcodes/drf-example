# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('first_name', models.CharField(max_length=100, blank=True, default='')),
                ('middle_name', models.CharField(max_length=100, blank=True, default='')),
                ('last_name', models.CharField(max_length=100)),
                ('phone_number', models.CharField(validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')], max_length=16, blank=True)),
                ('billing_address', models.CharField(max_length=512)),
                ('company_name', models.CharField(max_length=100, blank=True, default='')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='contacts')),
            ],
            options={
                'ordering': ('last_name', 'first_name', 'middle_name'),
            },
        ),
    ]
