from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from restexample.serializers import UserSerializer, GroupSerializer
    
from .models import Contact, Foo, Bar
from .serializers import ContactSerializer, FooSerializer, BarSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from logging import warn
from rest_framework import permissions

from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer, BrowsableAPIRenderer

from .permissions import IsOwnerOrReadOnly

from rest_framework.permissions import IsAdminUser

from django.contrib.auth.hashers import make_password

import pdb; 

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    permission_classes = (IsAdminUser,)

    def perform_create(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

    def perform_update(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer    

    permission_classes = (IsAdminUser,)
                          
class FooViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Foo.objects.all()
    serializer_class = FooSerializer    
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly)
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
    
'''
    test retrieve generic view
'''
class FooDetail(generics.RetrieveUpdateAPIView):
    """
    A view that returns a templated HTML representation of a given user.
    """
    queryset = Foo.objects.all()
    renderer_classes = (TemplateHTMLRenderer,)
    template_name='foo/foo_detail.html'
    serializer_class=FooSerializer
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()        
        serializer = FooSerializer(self.object)
        return Response({'foo': self.object, 'serializer':serializer}\
        , template_name='foo/foo_detail.html')
        
'''
Test generic list view
'''
class FooList(generics.ListCreateAPIView):
    queryset = Foo.objects.all()
    serializer_class = FooSerializer
    #renderer_classes = (TemplateHTMLRenderer,)
    #template_name='foo/foo_list_create.html'    
    
    
class BarViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Foo's Bar to be viewed or edited.
    """
    queryset = Bar.objects.all();
    serializer_class = BarSerializer

    
class CustomerViewSet(viewsets.ModelViewSet):
    """
    API Endpoint for Customer Data (Contacts)
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly)
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
#
# NOT USED
# for demonstration purposes
#

class ContactList(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly)
    
    queryset = Contact.objects.all()
    template_name="contact/contact_list.html"    
    renderer_classes = (TemplateHTMLRenderer, BrowsableAPIRenderer, JSONRenderer)
    serializer_class = ContactSerializer
    
    """
    List all contacts, or create a new contact.
    """
    def get(self, request, format=None):
        contacts = Contact.objects.all()
        serializer = ContactSerializer(contacts, many=True)
        if format == None or format == 'html':
            return Response({'contacts': contacts, 'serializer':serializer})
        else:
            return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ContactSerializer(data=request.data)
        u = request.user
        warn(str(serializer.is_valid())+" valid?"+" the user: " + (str(u)))
        if serializer.is_valid() and u:
            serializer.save(owner=u)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#
# NOT USED
# for demonstration purposes
#

class ContactDetail(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    queryset = Contact.objects.all()
    renderer_classes = (TemplateHTMLRenderer, BrowsableAPIRenderer, JSONRenderer, )
    template_name="contact/contact_detail.html"      
    
    """
    Retrieve, update or delete a contact instance.
    """
    def get_object(self, pk):
        try:
            return Contact.objects.get(pk=pk)
        except Contact.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        contact = self.get_object(pk)
        serializer = ContactSerializer(contact)
        if format == None:
            return Response({'contact': contact, 'serializer':serializer})
        else:
            return Response(serializer.data)
        
    def post(self, request, pk, format=None):
        contact = self.get_object(pk)
        warn("POST using renderer "  +request.accepted_renderer.format )
        serializer = ContactSerializer(contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            if format == None:
                 return Response({'contact': contact, 'serializer':serializer})
            else:
                return Response(serializer.data)
        warn("POST ERROR invalid serializer data " );
                
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        contact = self.get_object(pk)
        serializer = ContactSerializer(contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        contact = self.get_object(pk)
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)