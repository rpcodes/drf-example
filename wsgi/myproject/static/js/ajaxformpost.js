/**
 * Help function for POSTing JSON data to
 * an endpoint from a form. Uses JQuery form.serialize
 * under the hood
 */

function ajaxFormPost(url, processData, success, fail, always){
    // Use closure to return event handler that kicks off AJAX
    return function(event) {
        /* stop form from submitting normally */
        event.preventDefault();

        // TODO: If no callback for processData, do standard:
        /* get some values from elements on the page: */
        //var $form = $(this);//,
            // url = "/api/users/",
            //dd = $form.serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});
        var dd = processData($(this))
        // must format for JSON post
        dd = JSON.stringify(dd);
        
        /* Send the data using post */
        var posting = $.ajax( url
            , { jsonp : false
                , type : "POST"
                , dataType : 'json'
                , contentType : 'application/json'
                , data : dd
            } )
        .done(success)
        .fail(error)
        .always(function(data){
            console.log(url + " response: " + JSON.stringify(data))
        });
    };
};

// TODO when does this fail
function $formAsJSON($form) {
        return $form.serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});  
}