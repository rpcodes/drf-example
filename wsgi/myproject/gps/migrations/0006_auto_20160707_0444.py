# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gps', '0005_driver'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='driver',
            name='first_name',
        ),
        migrations.AddField(
            model_name='driver',
            name='color',
            field=models.CharField(max_length=7, default='#990000'),
            preserve_default=False,
        ),
    ]
