# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restexample', '0002_foo'),
    ]

    operations = [
        migrations.AddField(
            model_name='foo',
            name='might_be_blank',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='foo',
            name='required_woth_default',
            field=models.CharField(max_length=100, default='default'),
        ),
        migrations.AlterField(
            model_name='foo',
            name='foo',
            field=models.CharField(max_length=100),
        ),
    ]
