# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gps', '0003_auto_20160617_1931'),
    ]

    operations = [
        migrations.CreateModel(
            name='GPSLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('lat', models.FloatField()),
                ('lon', models.FloatField()),
            ],
        ),
        migrations.RemoveField(
            model_name='gpsupdate',
            name='id',
        ),
        migrations.RemoveField(
            model_name='gpsupdate',
            name='lat',
        ),
        migrations.RemoveField(
            model_name='gpsupdate',
            name='lon',
        ),
        migrations.AddField(
            model_name='gpsupdate',
            name='gpslocation_ptr',
            field=models.OneToOneField(to='gps.GPSLocation', parent_link=True, default=1, primary_key=True, auto_created=True, serialize=False),
            preserve_default=False,
        ),
    ]
